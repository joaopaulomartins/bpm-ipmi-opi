# This script is asigned to FRU sensor window to display widget
# Its role is to generate sensors name depending on PVs values
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model.properties import WidgetColor

# To write a portable script, check for the display builder's widget type:
display_builder = 'getVersion' in dir(widget)

if display_builder:  
    phoebus = 'PHOEBUS' in dir(ScriptUtil)
    if phoebus:
		from org.phoebus.framework.macros import Macros
    else:
		from org.csstudio.display.builder.model.macros import Macros
else:
    from org.csstudio.opibuilder.scriptUtil import PVUtil, ConsoleUtil

# function to get reference to display screen
def getDisplay():
	display = widget.getDisplayModel()
	return display

# get display reference
display = getDisplay()

# get macros to generate PVs names
macro = display.getEffectiveMacros()
pmacro = macro.getValue('P')
module_macro = macro.getValue('MODULE')
idx_macro = macro.getValue('IDX')
crate_macro = macro.getValue('CRATE_NUM')

# get number of sensors only when module is present
if PVUtil.getInt(pvs[1]):
	name = pmacro + module_macro + crate_macro + idx_macro + "SensorCnt"
	try:
		# create listener on PV
		pv1 = PVUtil.createPV(name, 5000)
		number_of_sensors = int(pv1.read().getValue())
		error_flag = 0
		# release listener
		PVUtil.releasePV(pv1)
	except:
		ScriptUtil.showErrorDialog(widget, "Can't read number of sensors from %s" % name)
		error_flag = 1

	if error_flag == 0:
		for i in range(0, number_of_sensors):
			# PV with sensor name
			sensor_name = pmacro + module_macro + crate_macro + idx_macro + "Sen" + str(i) + "Name"
			try:	
				# create listener on PV
				pv = PVUtil.createPV(sensor_name, 5000)
				# read PV
				name_str = pv.read().getValue()
				# relase listener
				PVUtil.releasePV(pv)
				# find label
				label = ScriptUtil.findWidgetByName(display, 'Label_' + str(i))
				# insert sensor name to label
				label.setPropertyValue('text', name_str)
			except:
				ScriptUtil.showErrorDialog(widget, "Something went wrong!")
				break